import React, { useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';

function UsersList(props) {
  const [students, setStudents] = useState([]);
  const [selecionado, setSelecionado] = useState(0);
  const [recarregar, setRecarregar] = useState(false);
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    const loadData = async () => {
      const students = await axios.get('http://localhost/alunos');
      setStudents(students.data);
    };

    loadData();
  }, [recarregar]);

  return (
    <>
      <div>
        <h1>{props.title || ''}</h1>
        {students.map((student) => {
          return (
            <div
              onClick={(e) => {
                e.preventDefault();
                if (selecionado === student._id) setSelecionado(0);
                else setSelecionado(student._id);
              }}
              className={`student ${
                selecionado === student._id ? 'selected' : ''
              }`}
              key={student.id}
            >
              Nome: {student.name}, Idade: {student.age}
              <br />
              ID: {student._id}
            </div>
          );
        })}
      </div>

      <Link to="/add">
        <button>Adicionar</button>
      </Link>

      <button
        onClick={() => {
          props.setId(selecionado);
          setRedirect(true);
        }}
      >
        Alterar
      </button>

      <button
        onClick={async () => {
          if (selecionado === 0) {
            alert('Nenhum usuário selecionado!');
            return;
          }
          const response = await axios.delete(
            `http://localhost/alunos/${selecionado}`
          );
          setRecarregar(!recarregar);
          alert(response.data);
        }}
      >
        Remover
      </button>

      {redirect && <Redirect to="/alterar" />}
    </>
  );
}

export default UsersList;
