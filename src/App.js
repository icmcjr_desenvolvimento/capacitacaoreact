import React, { useState } from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import PaginaInicial from './containers/PaginaInicial';
import AdicionarAluno from './containers/AdicionarAluno';

function App() {
  const [id, setId] = useState();
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <PaginaInicial setId={setId} />
          </Route>
          <Route path="/add">
            <AdicionarAluno />
          </Route>
          <Route path="/alterar">
            <AdicionarAluno id={id} />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
