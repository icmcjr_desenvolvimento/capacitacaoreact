import React, { useState, useEffect } from 'react';
import './styles.css';
import axios from 'axios';

function AdicionarAluno(props) {
  const [name, setName] = useState('');
  const [age, setAge] = useState(0);

  useEffect(() => {
    console.log(props.id);
    const loadData = async () => {
      const response = await axios.get(`http://localhost/alunos/${props.id}`);
      const aluno = response.data;
      setName(aluno.name);
      setAge(aluno.age);
    };
    if (props.id !== 0) {
      loadData();
    }
  }, [props.id]);

  const adicionar = async () => {
    const response = await axios.post('http://localhost/alunos', {
      name,
      age,
    });
    alert('cadastrado com sucesso!');
    console.log(response.data);
  };

  const alterar = async () => {
    const response = await axios.put(`http://localhost/alunos/${props.id}`, {
      name,
      age,
    });
    console.log(response.data);
    alert('Alterado com sucesso!');
  };

  return (
    <div className="container">
      <input
        onChange={(e) => {
          setName(e.target.value);
        }}
        placeholder="Name"
        value={name}
      ></input>
      <input
        onChange={(e) => {
          setAge(e.target.value);
        }}
        placeholder="Age"
        value={age}
      ></input>
      <button
        onClick={async () => {
          if (props.id && props.id !== 0) {
            alterar();
          } else {
            adicionar();
          }
        }}
      >
        {props.id && props.id !== 0 ? 'Alterar' : 'Adicionar'}
      </button>
    </div>
  );
}

export default AdicionarAluno;
