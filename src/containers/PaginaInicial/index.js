import React from 'react';
import UsersList from '../../components/UsersList';
import './styles.css';

function PaginaInicial(props) {
  return (
    <div className="container">
      <UsersList setId={props.setId} title={'Lista de alunos'} />
    </div>
  );
}

export default PaginaInicial;
